package vn.tiki.android.androidhometest.ui

import android.content.Context
import android.graphics.BitmapFactory
import android.os.CountDownTimer
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.deal_item.view.*
import vn.tiki.android.androidhometest.R
import vn.tiki.android.androidhometest.data.api.response.Deal


/**
 * Created by TamHuynh on 6/23/18.
 */
class DealAdapter() : RecyclerView.Adapter<DealAdapter.DealHolder>(), DealFragment.GridIsScroll  {

    override fun isScrolling(isScroll: Boolean) {
        this.isScroll = isScroll
    }

    var listDeal : MutableList<Deal> = ArrayList()
    private var mInflater: LayoutInflater? = null
    private var context : Context? = null
    private var countDownTimer : CountDownTimer? = null
    private var isScroll  = false

    constructor(listDeal : MutableList<Deal>, applicationContext : Context) : this() {
        this.listDeal = listDeal
        this.mInflater = LayoutInflater.from(applicationContext)
        this.context = applicationContext



        val timer: CountDownTimer = object : CountDownTimer(Int.MAX_VALUE.toLong(), 1000 ){
            override fun onFinish() {
            }

            override fun onTick(millisUntilFinished: Long) {
                var removeTemp = ArrayList<Deal>()

                this@DealAdapter.listDeal.forEachIndexed {
                    index,deal ->
                    var timeExpired = deal.timeExpired - 1
                    if(timeExpired >= 0){
                        deal.timeExpired = timeExpired
                    }else{
                        removeTemp.add(deal)
                    }
                }
                this@DealAdapter.listDeal.removeAll(removeTemp)
                if(!isScroll)
                    this@DealAdapter.notifyDataSetChanged()
            }

        }
        timer.start()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DealAdapter.DealHolder {
        var view = mInflater?.inflate(R.layout.deal_item, parent, false)
        return DealHolder(view)
    }

    override fun getItemCount(): Int {
        return listDeal.size
    }

    override fun onBindViewHolder(holder: DealHolder, position: Int) {
        var dataDeal = listDeal[position]
        holder.itemView.tv_name.text = dataDeal?.productName
        holder.itemView.tv_price.text = dataDeal?.productPrice.toString() + " VND"
        holder.itemView.tv_counter.text = "Promo expired in : " + dataDeal?.timeExpired.toString() + " s"

        var image = dataDeal?.productThumbnail?.split("android_asset/")?.get(1)
        val inputString = this.context?.assets?.open(image)
        val bitmap = BitmapFactory.decodeStream(inputString)
        holder.itemView.imageView.setImageBitmap(bitmap)
    }


    class DealHolder(itemView : View?) : RecyclerView.ViewHolder(itemView) {
        init {
        }
    }

    fun finishTimer(){
        countDownTimer?.cancel()
    }


}