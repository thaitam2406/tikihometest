package vn.tiki.android.androidhometest.ui

import android.annotation.SuppressLint
import android.app.Fragment
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_list_deal.*
import vn.tiki.android.androidhometest.R
import vn.tiki.android.androidhometest.data.api.ApiServices
import vn.tiki.android.androidhometest.data.api.response.Deal
import vn.tiki.android.androidhometest.di.initDependencies
import vn.tiki.android.androidhometest.di.inject
import vn.tiki.android.androidhometest.di.releaseDependencies
import java.util.*
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.SCROLL_STATE_IDLE


/**
 * Created by TamHuynh on 6/23/18.
 */
class DealFragment : Fragment() {

    val apiServices by inject<ApiServices>()
    var adapterDeal : DealAdapter ? = null
    override fun onStart() {
        super.onStart()
        initDependencies(activity.baseContext)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return LayoutInflater.from(activity).inflate(R.layout.fragment_list_deal,container)
    }

    override fun onResume() {
        super.onResume()

        object : AsyncTask<Unit, Unit, List<Deal>>() {
            override fun doInBackground(vararg params: Unit?): List<Deal> {
                var  result: List<Deal> = apiServices.getDeals()

                result.forEach { deal ->
                    var endDateTimeSecond = deal.endDate.time
                    var currentSecond = Calendar.getInstance().time.time
                    var expiredTime = endDateTimeSecond - currentSecond
                    deal.timeExpired = expiredTime/1000
                }

                return result
            }

            override fun onPostExecute(result: List<Deal>?) {
                super.onPostExecute(result)
                result.orEmpty()
                        .forEach { deal ->
                            println(deal.productName)
                        }
                bindAdapter(result)




            }
        }.execute()
    }

    private fun bindAdapter(result: List<Deal>?) {
        if(adapterDeal == null) {
            adapterDeal = DealAdapter(result?.toMutableList() ?: arrayListOf(), activity.applicationContext)
            var layoutManager = GridLayoutManager(activity.applicationContext,2)
            recycler_deal_list.layoutManager = layoutManager
            recycler_deal_list.setHasFixedSize(true)
            recycler_deal_list.adapter = adapterDeal
            recycler_deal_list.adapter.notifyDataSetChanged()
            recycler_deal_list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    if (newState == SCROLL_STATE_IDLE) {
                        if (adapterDeal != null) {
                            adapterDeal!!.isScrolling(false)
                        }
                    } else {
                        if (adapterDeal != null) {
                            adapterDeal!!.isScrolling(true)
                        }
                    }
                }
            })
        }else{
            adapterDeal?.listDeal?.clear()
            adapterDeal?.listDeal = result?.toMutableList() ?: arrayListOf()
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        releaseDependencies()
        adapterDeal?.finishTimer()
    }

    interface GridIsScroll{
        fun isScrolling(isScroll : Boolean)
    }
}